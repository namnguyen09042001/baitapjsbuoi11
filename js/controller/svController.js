function layThongTinTuForm() {
  // lay thong tin tu form
  const _maSv = document.getElementById("txtMaSV").value;
  const _tenSv = document.getElementById("txtTenSV").value;
  const _email = document.getElementById("txtEmail").value;
  const _matKhau = document.getElementById("txtPass").value;
  const _diemToan = document.getElementById("txtDiemToan").value;
  const _diemLy = document.getElementById("txtDiemLy").value;
  const _diemHoa = document.getElementById("txtDiemHoa").value;
  // tạo object sv
  var sv = {
    ma: _maSv,
    ten: _tenSv,
    matKhau: _matKhau,
    email: _email,
    diemToan: _diemToan,
    diemLy: _diemLy,
    diemHoa: _diemHoa,
  };

  return sv;
}

function renderDSSV(svArr) {
  // render dssv ra table
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    var contentTr = `<tr>
                          <td>${item.ma}</td>
                          <td>${item.ten}</td>
                          <td>${item.email}</td>
                          <td>0</td>
                          <td> 
                          <button onclick="deleteSV('${item.ma}')" class="btn btn-danger" > xóa </button>
                            </td>
                            <td> 
                          <button onclick="editSV('${item.ma}')" class="btn btn-warning" > Sửa</button>
                            </td>
                          
                       </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  // tìm vị trí
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    var sv = arr[index];
    if (sv.ma == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}

function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;

  document.getElementById("txtTenSV").value = sv.ten;

  document.getElementById("txtEmail").value = sv.email;

  document.getElementById("txtPass").value = sv.matKhau;

  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;

  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

var dssv = [];
var DSSV = [];

// lấy dữ liệu từ localStorage
//  object lấy từ localStorage sẽ bị mất các key chứ function ( method )
var dssvJson = localStorage.getItem(DSSV);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
}
renderDSSV(dssv);

function addSV() {
  var sv = layThongTinTuForm();

  dssv.push(sv);
  // lưu vào localStorage ( không bị mất dữ liệu khi load trang )

  // convert array dssv thành json
  var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
  localStorage.setItem(DSSV, dssvJson);
  renderDSSV(dssv);
}

function deleteSV(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  if (viTri != -1) {
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
  }
  // lưu vào localStorage ( không bị mất dữ liệu khi load trang )

  // convert array dssv thành json
  //   var dssvJson = JSON.stringify(dssv);
  // lưu data json vào localStorage
  //   localStorage.setItem(DSSV, dssvJson);
}

function editSV(idSv) {
  var viTri = timKiemViTri(idSv, dssv);
  if (viTri == -1) {
    return;
  }

  var sv = dssv[viTri];
  //   show thông tin lên form
  showThongTinLenForm(sv);
}
function updateSV() {
  console.log("🚀 ~ file: main.js:50 ~ updateSV ~ updateSV", updateSV);
  var sv = layThongTinTuForm();
  var viTri = timKiemViTri(sv.ma, dssv);
  if (viTri != -1) {
    dssv[viTri] = sv;
    renderDSSV(dssv);
  }
}
